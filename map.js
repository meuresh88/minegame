(function() {
  	"use strict";
  	var map = function(minedLand,secureLand,$q){
  		var map = function(max) {
			this.max = max;
			this.area = [];
			this.x = 0;
			this.y = 0;
		}

		map.prototype.createMap = function() {

			
				
		};


		/*
		fill secure lands and number horizontally
		*/
		map.prototype._horizontalAdd = function() {

			if(this._validateXYinAdd(true)) { // validate and increse golbal x,y cordinates
				return;
			}

			if(this.area[this.x][this.y] == null){
				this.area[this.x][this.y] = new new secureLand();
			}else if(this.area[this.x][this.y].isMined){
				if(this.x-1>0)
					this.area[this.x-1][this.y].number += 1;
				if(this.x+1 < this.max){
					this.area[this.x + 1][this.y] = new new secureLand();
					this.area[this.x + 1][this.y].number += 1;
				}

			}
			this.x += 1;
			this._horizontalAdd();
				
		};

		/*
		fill secure lands and number vertically
		*/
		map.prototype._verticalAdd = function() {

			if(this._validateXYinAdd(false)) { // validate and increse golbal x,y cordinates
				return;
			}

			if(this.area[this.x][this.y].number>0) {
				if(this.y-1>0 && !this.area[this.x][this.y].isMined)
					this.area[this.x][this.y-1].number += 1;
				if(this.y+1 < this.max && !this.area[this.x][this.y].isMined){
					this.area[this.x][this.y+1].number += 1;
				}
				
			}else if(this.area[this.x][this.y].isMined){
				if(this.y-1>0 && !this.area[this.x][this.y].isMined)
					this.area[this.x][this.y-1].number += 1;
				if(this.x+1 < this.max && !this.area[this.x][this.y].isMined){
					this.area[this.x][this.y+1] = new new secureLand();
					this.area[this.x][this.y+1].number += 1;
				}

			}
			
			this.y +=1;
			
			this._verticalAdd();
		};

		/*
		validate if the x or y has exceeded max val
		*/
		map.prototype._validateXYinAdd = function(isHorizaontalAdd) {
			if( this.x == this.max-1 && this.y == this.max-1 ){
				return true;
			}else if( this.x == this.max-1 && isHorizaontalAdd){
				this.x=0;
				this.y+=1;
				return false;
			}else if( this.y == this.max-1 && !isHorizaontalAdd){
				this.x+=1;
				this.y==0;
				return false;
			}

		};
  	}
	
  	secureLand.$inject = ['minedLand','secureLand','$q'];
  	angular
  	.module('mineswepper')
  	.factory('secureLand',secureLand);
  })()

