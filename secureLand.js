(function() {
  	"use strict";
  	var secureLand = function(land){
  		var secureLand = function(){
  			land.apply(this)
			this.isMined = false;
			this.number = 0;
		}

		secureLand.prototype = Object.create(land.prototype);
		secureLand.prototype.constructor = secureLand
		secureLand.prototype.showNumber = function(){
			return this.number;
		}

		return secureLand;
  	}
  	secureLand.$inject = ['land'];
  	angular
  	.module('mineswepper')
  	.factory('secureLand',secureLand);
  })()

