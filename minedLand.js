(function() {
  	"use strict";

    var minedLand = function(land){
      var minedLand = function(){
        land.apply(this)
        this.isMined=true;
        this.number = -1;
      }

      minedLand.prototype = Object.create(land.prototype);
      minedLand.prototype.constructor = minedLand;


      return minedLand;
    }
    minedLand.$inject = ['land'];
  	angular
  	.module('mineswepper')
  	.factory('minedLand',minedLand);
  })()

