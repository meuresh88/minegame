
(function() {
  	"use strict";
  	angular
  	.module('mineswepper')
  	.factory('utility',function(){
  		/*
		common utilities lies here
		*/
		var utility = function(){

		}
		utility.prototype={
			/*
			generate random int number which the upper limit celing value is (maximumNum-1)
			*/
			randomNumberGenerator : function(maximumNum){
				return parseInt( Math.random()*maximumNum );
			},

			/*
			set number of mines avaliable in the map depends on the difficulty level
			*/
			setMineNumber : function(singleSide,difficulty,levels){
				var difficultAvailable  =  levels.indexOf(difficulty);
				if(difficultAvailable!= -1){
					return parseInt((singleSide*singleSide)/4)*difficulty; //low difficulty == low mines
				}else{
					return parseInt((singleSide*singleSide)/4)*3; // take the maximum difficulty level if level is not set
				}
		}
		}
		return utility;
  	});
  })()





