(function() {
  	"use strict";

  	var game = function(utility,minedLand,secureLand,$q){
		
		/*
		logics in the game lies here.
		functions available: generate the map, walk through the land ,....
		*/
		var game = function(singleSide,difficulty){
			this.area = [];
			this.singleSide = singleSide;
			this.difficulty = parseInt(difficulty);
			this.levels=[1,2,3];
			this.util= new utility();
			this.numberOfMines = this.util.setMineNumber(singleSide,difficulty,this.levels);
			this.remainingMines = this.numberOfMines;
			this._x = 0;
			this._y = 0;

		}
		game.prototype.constructor = game;

		//returns empty map 
		game.prototype.createMap = function() {
			var deferred = $q.defer();

			for (var i = 0; i < this.singleSide; i++) {
				this.area[i] = new Array();
			}
			this.randomPlaceMine();
			deferred.resolve(this);
			return deferred.promise;
		};

		
		game.prototype.explode = function(){
			
			for(var x=0;x<this.singleSide;x++){
					for(var y =0; y<this.singleSide; y++){
						if(this.area[x][y].isMined){
							this.area[x][y].explore();
						}
					}
				}
		}

		//step to the land 
		game.prototype.stepLand = function(intX, intY){
			if(intY>= this.singleSide || intX>= singleSide){
				throw 'not inside area error';
			}
			return area[intX][intY].explore();
		}

		//randomly place mines
		game.prototype.randomPlaceMine = function(){
			//var deferred = $q.defer();
			for(var i=0;i<this.numberOfMines;i++){
				if(this.area[this.util.randomNumberGenerator(this.singleSide)][this.util.randomNumberGenerator(this.singleSide)] == null){
					var mine = new minedLand();
					this.area[this.util.randomNumberGenerator(this.singleSide)][this.util.randomNumberGenerator(this.singleSide)] = mine;
					this.remainingMines --;
				}
			}
			
			if(this.remainingMines>0){
				for(var x=0;x<this.singleSide;x++){
					for(var y =0; y<this.singleSide; y++){
						if(this.area[this.util.randomNumberGenerator(this.singleSide)][this.util.randomNumberGenerator(this.singleSide)] == null){
							var mine = new minedLand();
							this.area[this.util.randomNumberGenerator(this.singleSide)][this.util.randomNumberGenerator(this.singleSide)] = mine;
							this.remainingMines --;
							if(this.remainingMines==0){
								//deferred.resolve(this);
								return this;
							}
						}
					}
				}
			}
			return this;
			//deferred.resolve(this);
			//return deferred.promise;
		}


		//get the map
		game.prototype.getFullMap = function(){
			return this.createMap()
			.then(function(game){
				return game.randomPlaceMine();
				
			})
			.then(function (game) {
				return game.numberMap();
			});
			
		}

		game.prototype.numberMap = function(){
			console.log(this)
			//var deferred = $q.defer();

			for (var x = 0; x < this.singleSide; x++) {
				for (var y = 0; y < this.singleSide; y++) {
					if(this.area[x][y] == null){
						this.area[x][y] = new secureLand();
					}else if(this.area[x][y].isMined){ //todo: come up with efficient readable algorithm
						
					try{
						if(x+1<this.singleSide && this.area[x+1][y] == null ){
							this.area[x+1][y] = new secureLand();
							this.area[x+1][y].number+=1;
						
						}else if(x+1<this.singleSide && !this.area[x+1][y].isMined){
							this.area[x+1][y].number+=1;
						
						}else{
							
						}

						if( x-1>=0 && this.area[x-1][y] == null ){
							this.area[x-1][y] = new secureLand();
							this.area[x-1][y].number+=1;
						
						}else if(x-1>=0 && !this.area[x-1][y].isMined){
							this.area[x-1][y].number+=1;
						
						}else{
						
						}

						if( y+1<this.singleSide && this.area[x][y+1] == null ){
							this.area[x][y+1] = new secureLand();
							this.area[x][y+1].number+=1;
						
						}else if(y+1<this.singleSide && !this.area[x][y+1].isMined){
							this.area[x][y+1].number+=1;
						
						}else{
						}

						if(y+1<this.singleSide && x+1<this.singleSide && this.area[x+1][y+1] == null){
							this.area[x+1][y+1] = new secureLand();
							this.area[x+1][y+1].number+=1;
						
						}else if(y+1<this.singleSide && x+1<this.singleSide && !this.area[x+1][y+1].isMined){
							this.area[x+1][y+1].number+=1;
						
						}else{
							
						}

						if( y+1<this.singleSide && x-1>=0 && this.area[x-1][y+1] == null ){
							this.area[x-1][y+1] = new secureLand();
							this.area[x-1][y+1].number+=1;
						
						}else if(y+1<this.singleSide && x-1>=0 && !this.area[x-1][y+1].isMined){
							this.area[x-1][y+1].number+=1;
						
						}else{
							
						}

						if( y-1>=0 && this.area[x][y-1] == null){
							this.area[x][y-1] = new secureLand();
							this.area[x][y-1].number+=1;
						
						}else if(y-1>=0 && !this.area[x][y-1].isMined){
							this.area[x][y-1].number+=1;
						
						}else{
							
						}

						if( y-1>=0 && x-1>=0 && this.area[x-1][y-1] == null){
							this.area[x-1][y-1] = new secureLand();
							this.area[x-1][y-1].number+=1;
						
						}else if(y-1>=0 && x-1>=0 && !this.area[x-1][y-1].isMined){
							this.area[x-1][y-1].number+=1;
						
						}else{
							
						}

						if(y-1>=0 && x+1<this.singleSide && this.area[x+1][y-1] == null ){
							this.area[x+1][y-1] = new secureLand();
							this.area[x+1][y-1].number+=1;
						
						}else if(y-1>=0 && x+1<this.singleSide && !this.area[x+1][y-1].isMined){
							this.area[x+1][y-1].number+=1;
						
						}else{
							
						}
					}catch(e){
						console.log(e)
					}
						
						
					}
				}
			}
			console.log(this.area)
			return this.area;
			//deferred.resolve(this.area);
			//return deferred.promise;
		}

		return game;
	}
	game.$inject = ['utility','minedLand','secureLand','$q'];
  	angular
  	.module('mineswepper')
  	.factory('gameFactory',game);
	
	
	}

)()
