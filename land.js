/*
main concept: there can be two types of lands ==> land with mines and secure lands.
*/

(function() {
  	"use strict";
  	angular
  	.module('mineswepper')
  	.factory('land',function(){
  		var land = function(){ 
			this.isDiscovered = false; 
			this.isFlagged = false;
		}

		land.prototype ={
			constructor : land,
			flag : function(){
				this.isFlagged = true;
			},
			unflag : function(){
				this.isFlagged = false;
			},
			explore : function(){
				console.log("isFlagged : "+this.isFlagged);
				if(!this.isFlagged){
					this.isDiscovered = true; 
					return this.isMined;
				}
			}
		}
		return land;
  	});
  })()


