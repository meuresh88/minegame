(function() {
  	"use strict";
  	
  	var gameController = function($scope,gameFactory,$q){
		var game = new gameFactory(5,1);
		$scope.map = game.getFullMap().then(function(data){
			$scope.map = data;
		});
		$scope.explode = function(land){
			if(land.isMined){
				game.explode();
			}
		}
		
	}


  	gameController.$inject = ['$scope','gameFactory','$q'];
	angular.module('mineswepper').controller('gameController',gameController);
	
	
	

})()







